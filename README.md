# GP_DE_solver
A (ordinary) differential equation solver using genetic programming

**NOTE: This project has been discontinued from my side in favour of [Genetic_DE_Solver](https://github.com/johanbluecreek/Genetic_DE_solver) using [Julia](https://github.com/JuliaLang/julia).**

## Introduction

This implementation follows the outline of

 **[TL]** TSOULOS, Ioannis G. et LAGARIS, Isaac E. "*Solving differential equations with genetic programming*". Genetic Programming and Evolvable Machines, 2006, vol. 7, no 1, p. 33-54.

for a differential equations solver using genetic programming but in Python, using [SymPy](https://github.com/sympy/sympy) for evaluating derivatives.

## Repository content

 * `GP_DE_solver.py`: module file.
 * `GP_DE_solver-Benchmarking.ipynb`: [Jupyter](http://jupyter.org/)-notebook attempting to mimic the experiments carried out in **[TL]**.
 * `GP_DE_solver-Guide.ipynb`: [Jupyter](http://jupyter.org/)-notebook guide for the content of `GP_DE_solver.py` and how to use it.

## Usage

The most basic usage is to execute

 `$ python GP_DE_solver.py`

which will solve the ODE assigned as the default. To get access to the module, execute the following in a python shell

 `>>> import GP_DE_solver`

See `GP_DE_solver-Guide.ipynb` for more detailed explanation.

## TODO/Known issues/Limitations

The implementation differs from the outline of **[TL]** in some ways
 * The "tournament" parent-selection is made only optional here since it is quite slow, and a "random" parent-selection is used by default.
 * The chromosomes have a dynamical length, with a hard-coded cap at 50 during population generation (used in **[TL]**), and the cap is increased to 60 during execution/reevaluation.
 * It only supports single (e.g. no systems) ODEs.
 * Slight difference to the grammar used.

It should be noted that the module is *very* slow. This is also the main TODO-issue: Speed it up. It is currently too slow to replicate the experiments conducted in **[TL]**.

Other TODO-items are

 * Write in all ODE and NODE experiments conducted in **[TL]** into `GP_DE_solver-Benchmarking.ipynb`.
 * Write doc-strings for everything.
 * Code clean up.

Other, minor, TODO-items can be found as comments in the code.
