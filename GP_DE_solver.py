
# Imports
from sympy import *
#TODO: Only import the necrssary above
import re
from random import randint, random, sample
from numpy import linspace
import time
import copy
import multiprocessing
import signal
from functools import partial
from contextlib import contextmanager

class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


#TODO: How to make these be given as input?
exprs = ['<exprs><op><exprs>', '<digit>', '(<exprs>)', '<func>(<exprs>)', 'x']
#using the below line causes all sorts of overflow errors very frequently.
#exprs = ['<exprs><op><exprs>', '<digit>', '<func>(<exprs>)']
op = ['+', '-', '*', '/']
#func = ['sin', 'cos', 'exp', 'log']
#digit = ['%i' % i for i in range(0,10)]
func = ['sin', 'cos', 'exp', 'log', 'Integer(1)*']
digit = ['%i' % i for i in range(0,10)] + ['pi']

# Prerequisite functions for Chromosome
def bad_set(inset):
    return (zoo in inset) or (oo in inset) or (I in inset) or (nan in inset)

def gen_chromo(entry_range, depth=0):
    regexp = re.compile("(?<=(\<)).*?(?=\>)")
    chromo = []
    flat_clist = []
    thestring = "<exprs>"
    max_size = 40
    while (not (regexp.search(thestring) == None)) and len(chromo) < max_size:
        thestring = "<exprs>"
        chromo.append(randint(0,entry_range))
        for part in chromo:
            try:
                match = regexp.search(thestring).group(0)
                thestring = eval("thestring.replace('<{e}>', {e}[{c}%len({e})], 1)".format(e=match, c=part))
                flat_clist = flat_clist + [eval("{c}%len({e})".format(e=match, c=part))]
            except:
                None
    # Incase we have exceeded max_size and thestring is unfinished, start over.
    #TODO: Can cause RuntimeError due to recusion depth, find workaround.
    if not (regexp.search(thestring) == None):
        if depth < 100:
            chromo, flat_clist, thestring = gen_chromo(entry_range, depth=depth+1)
        else:
            chromo, flat_clist, thestring = [4], [4], 'x'
    return chromo, flat_clist, thestring

def regen_chromo(entry_range, chromo, depth=0):
    flat_clist = []
    thestring = '<exprs>'
    iterator=0
    regexp = re.compile("(?<=(\<)).*?(?=\>)")
    max_size = 60
    while (not (regexp.search(thestring) == None)) and len(chromo) < max_size:
        match = regexp.search(thestring).group(0)
        length_increase = False
        try:
            thestring = eval("thestring.replace('<{e}>', {e}[{c}%len({e})], 1)".format(e=match, c=chromo[iterator]))
        except IndexError:
            length_increase = True
            chromo.append(randint(0,entry_range))
            thestring = eval("thestring.replace('<{e}>', {e}[{c}%len({e})], 1)".format(e=match, c=chromo[iterator]))
        flat_clist = flat_clist + [eval("{c}%len({e})".format(e=match, c=chromo[iterator]))]
        iterator += 1
    try:
        if not (regexp.search(thestring) == None):
            if depth < 100:
                chromo, flat_clist, thestring, length_increase, iterator = regen_chromo(entry_range, chromo, depth=depth+1)
            else:
                chromo, flat_clist, thestring, length_increase, iterator = [4], [4], 'x', 0, 0
    except RuntimeError:
        if depth < 100:
            chromo, flat_clist, thestring = gen_chromo(entry_range, depth=depth+1)
            chromo, flat_clist, thestring, length_increase, iterator = regen_chromo(entry_range, chromo, depth=depth+1)
        else:
            chromo, flat_clist, thestring, length_increase, iterator = [4], [4], 'x', 0, 0
    return chromo, flat_clist, thestring, length_increase, iterator

# Chromosome class
class Chromosome(object):
    def __init__(self, entry_range):
        #XXX: Moving this out to a separate function to... I don't know. Debug somehow.
        #regexp = re.compile("(?<=(\<)).*?(?=\>)")
        #chromo = []
        #flat_clist = []
        #thestring = "<exprs>"
        #while not (regexp.search(thestring) == None):
        #    thestring = "<exprs>"
        #    chromo.append(randint(0,entry_range))
        #    for part in chromo:
        #        try:
                    # The time_limit() here breaks it completely.
                    #with time_limit(0.1): #Unless this is fast, it will be a very big cromosome
        #            match = regexp.search(thestring).group(0)
        #            thestring = eval("thestring.replace('<{e}>', {e}[{c}%len({e})], 1)".format(e=match, c=part))
        #            flat_clist = flat_clist + [eval("{c}%len({e})".format(e=match, c=part))]
        #        except:
        #            None

        chromo, flat_clist, thestring = gen_chromo(entry_range)

        self.chromo_list = chromo

        self.complexity = len(chromo)

        self.eval = thestring

        try:
            self.expr = eval('lambda x: ' + self.eval)(Symbol('x'))
        except:
            self.expr = zoo

        self.flat_clist = flat_clist

        self.entry_range = entry_range

        # Will be defined later
        self.de = None
        self.bc = None

        self.error = None
        self.penalty = None
        self.fitness = None

        # For statistics and similar
        self.mutations = 0
        self.crossovers = 0

    def __add__(self, other):
        # Use __add__ for crossover
        new1 = copy.copy(self)
        new2 = copy.copy(other)

        if len(new1.chromo_list) > len(new2.chromo_list):
            rand = randint(0,len(new2.chromo_list)-1)
        else:
            rand = randint(0,len(new1.chromo_list)-1)

        # One-point crossover:
        new1.chromo_list = self.chromo_list[0:rand]+other.chromo_list[rand::]
        new2.chromo_list = other.chromo_list[0:rand]+self.chromo_list[rand::]
        #TODO: Implement a way to make a choice of crossover-operator

        # reevaluate from the new lists
        new1.regen_eval()
        new1.crossovers = new1.crossovers + 1
        new2.regen_eval()
        new2.crossovers = new2.crossovers + 1

        return [new1, new2]

    def __eq__(self, other):
        # Two are equal if the following is true.
        # Used to remove duplicates and keep diversity high
        return (True if ((self.flat_clist == other.flat_clist) or (self.eval == other.eval) or (self.expr == other.expr)) else False)

    def crossover(self, other):
        # Long-hand for crossover
        return self + other

    def regen_eval(self):
        # Call this if self.chromo_list has been changed manually
        #chromo = self.chromo_list
        #flat_clist = []
        #thestring = '<exprs>'
        #iterator=0
        #regexp = re.compile("(?<=(\<)).*?(?=\>)")
        #while not (regexp.search(thestring) == None):
        #    match = regexp.search(thestring).group(0)
        #    length_increase = False
        #    try:
        #        thestring = eval("thestring.replace('<{e}>', {e}[{c}%len({e})], 1)".format(e=match, c=chromo[iterator]))
        #    except IndexError:
        #        length_increase = True
        #        chromo.append(randint(0,self.entry_range))
        #        thestring = eval("thestring.replace('<{e}>', {e}[{c}%len({e})], 1)".format(e=match, c=chromo[iterator]))
        #    flat_clist = flat_clist + [eval("{c}%len({e})".format(e=match, c=chromo[iterator]))]
        #    iterator += 1
        chromo, flat_clist, thestring, length_increase, iterator = regen_chromo(self.entry_range, self.chromo_list)

        self.flat_clist = flat_clist
        if not length_increase:
            try:
                chromo[iterator]
                self.chromo_list = chromo[:iterator]
                self.complexity = len(chromo[:iterator])
            except IndexError:
                None
        else:
            self.chromo_list = chromo
            self.complexity = len(chromo)
        self.eval = thestring
        try:
            with time_limit(10):
                self.expr = eval('lambda x: ' + self.eval)(Symbol('x'))
        except:
            self.expr = zoo

    def mutate(self, m_chance=0.3):
        # Mutate a Chromosome
        mut = copy.copy(self)
        new_chromo = []
        for chrome in mut.chromo_list:
            if random() < m_chance:
                if random() >= 0.5:
                    new_chromo.append(chrome+1)
                else:
                    new_chromo.append(chrome-1)
            else:
                new_chromo.append(chrome)
        mut.chromo_list = new_chromo
        mut.regen_eval()
        if not mut == self:
            mut.mutations = mut.mutations + 1
        else:
            try:
                mut = mut.mutate(m_chance)
            except:
                None
        return mut

    def calc_fitness(self, de, bc, interval, var, pe_balance):

        fit = copy.copy(self)

        number = 10
        rangepoints = linspace(interval[0], interval[1], number)

        large = 10**10

        try:
            with time_limit(10):
                fit.expr = eval('lambda x: ' + fit.eval)(Symbol('x'))
                fit.de = eval('lambda x: ' + de.format(e=fit.eval))(Symbol('x'))
                fit.bc = eval('lambda x: ' + bc.format(e=fit.eval))(Symbol('x'))

                shape_error = eval('lambda x: ' + 'diff(' + de.format(e=fit.eval) + ', x)')(Symbol('x'))
        except:
            fit.expr = zoo
            fit.de = zoo
            fit.bc = zoo

            shape_error = zoo

        try:
            with time_limit(10):
                is_bad = bad_set((N(fit.de)).atoms()) or bad_set((N(fit.bc)).atoms()) or bad_set((N(fit.expr)).atoms()) or bad_set((N(shape_error)).atoms())
        except:
            is_bad = True
        if is_bad:
            fit.error = large
            fit.penalty = large

            shape_error = large

            fit.fitness = fit.error + fit.penalty + shape_error
        else:
            try:
                with time_limit(10):
                    tmp = [N(fit.de.subs(Symbol('x'), point)) for point in rangepoints]
                    tmp = [N(e**2) for e in tmp]
                    tmp = N(sum(tmp))

                    shape_error = [N(shape_error.subs(Symbol('x'), point)) for point in rangepoints]
                    shape_error = [N(e**2) for e in shape_error]
                    shape_error = N(sum(shape_error))
            except:
                tmp = large
                shape_error = large
            fit.error = tmp
            try: # SymPy throws a KeyError sometimes here
                fit.penalty = pe_balance*N(N(fit.bc)**2)
            except:
                fit.penalty = large

            if bad_set(N(fit.error).atoms()) or bad_set(N(fit.penalty).atoms()) or bad_set(N(shape_error).atoms()):
                fit.error = large
                fit.penalty = large
                shape_error = large
                fit.fitness = fit.error + fit.penalty #+ shape_error
            else:
                fit.fitness = fit.error + fit.penalty #+ shape_error
        return fit

# General functions

def sort_pop(pop):
    return sorted(pop, key=lambda mem: mem.fitness)

def do_crossover(i, mems):
    return (mems[i] + mems[i+1])

def do_mutate(mem, mutc=0.3):
    return mem.mutate(mutc)

def do_fitness(mem, de="diff({e}, x) - 1/(2*({e}))", bc="({e} - Integer(1)).subs(x,1)", interval=(1,4), var='x', pe_balance=100):
    return mem.calc_fitness(de, bc, interval, var, pe_balance)

# Generate population

def gen_pop(de="diff({e}, x) - 1/(2*({e}))", bc="({e} - Integer(1)).subs(x,1)", interval=(1.0,4.0), var='x', pe_balance=100, pop_size=1000, entry_range=255):
    pop = []
    for i in range(pop_size):

        tmp = Chromosome(entry_range)
        while tmp in pop:
            tmp = Chromosome(entry_range)
        tmp = tmp.calc_fitness(de, bc, interval, var, pe_balance)
        pop.append(tmp)
        del tmp

    pop = sort_pop(pop)
    return pop

def one_iter(population, cores=1, p_selection="random", de="diff({e}, x) - 1/(2*({e}))", bc="({e} - Integer(1)).subs(x,1)", interval=(1,4), var='x', pe_balance=100, pop_size=1000, rep_rate=0.2, m_chance=0.3):
    pop = []

    # Crossover
    ## Parent selection
    # "random" is selected as default because "tournament" is too slow.
    if p_selection.lower() == "random":
        if (1-rep_rate)*(pop_size) % 2 == 0:
            samp = sample(population,int((1-rep_rate)*(pop_size)))
        else:
            samp = sample(population,int((1-rep_rate)*(pop_size))+1)
    elif p_selection.lower() == "tournament":
        samp = []
        while len(samp) < (1-rep_rate)*(pop_size):
            s1 = sample(population,randint(2,pop_size))
            s1 = sort_pop(s1)[0]
            s2 = sample(population,randint(2,pop_size))
            s2 = sort_pop(s2)[0]
            if not s1 == s2:
                samp.append(s1)
                samp.append(s2)
    ## Crossover operation
## The "cores" option have been disabled for now since it is too slow.
#    if cores > 1:
#        pool = multiprocessing.Pool(cores)
#        tmp = pool.map(partial(do_crossover, mems=samp), range(0,len(samp),2))
#        tmp = list(tmp)
#        pool.close()
#        pool.join()
#        tmp = [item for sublist in tmp for item in sublist]
#        pop = pop + tmp
#    else:
#        for i in range(0,len(samp),2):
#            pop = pop + (samp[i] + samp[i+1])
    for i in range(0,len(samp),2):
        pop = pop + (samp[i] + samp[i+1])

    # Mutation
## The "cores" option have been disabled for now since it is too slow.
#    if cores > 1:
#        pool = multiprocessing.Pool(6)
#        tmp = pool.map(partial(do_mutate, mutc=m_chance), population)
#        pop = pop + list(tmp)
#        pool.close()
#        pool.join()
#    else:
#        pop = pop + [mem.mutate(m_chance) for mem in population]
    pop = pop + [mem.mutate(m_chance) for mem in population]

    # Calculate fitness
    if cores > 1:
        pool = multiprocessing.Pool(6)
        tmp = pool.map(partial(do_fitness, de=de, bc=bc, interval=interval, var=var, pe_balance=pe_balance), pop)
        pop = list(tmp)
        pool.close()
        pool.join()
    else:
        pop = [mem.calc_fitness(de, bc, interval, var, pe_balance) for mem in pop]

    # Remove equals
    pop = population + pop
    new_pop = []
    for mem in pop:
        if not mem in new_pop:
            new_pop.append(mem)
    pop = new_pop

    # Sort and truncate
    pop = sort_pop(pop)
    pop = pop[:pop_size]

    return pop



if __name__ == '__main__':
    print("Running this file, as you have done, will initiate a pretend run.")

    print("Please wait while a population is generated...")
    population = gen_pop()

    print("Expression with highest fitness: ", population[0].eval)
    print("Expression with lowest fitness: ", population[-1].eval)

    print("Please wait while one iteration is performed...")
    population = one_iter(population, cores=6)

    print("The population have now changed, so the following might differ now.")
    print("Expression with highest fitness: ", population[0].eval)
    print("Expression with lowest fitness: ", population[-1].eval)

    print("From here on, we canstart running until a termination condition.")

    stop = 100000
    ite = 0
    prec = 10**(-20)

    print("If iterations reach %i or fitness falls under %f, the loop will stop." % (stop, prec))

    while ite < stop and population[0].fitness > prec:
        population = one_iter(population, cores=6)
        ite = ite + 1

    print("The loop has now terminated.")
    print("This is the best the solution got: ", population[0].expr)
    print("Which should be: sqrt(x)")
    print("Since that is the solution of 'diff({e}, x) - 1/(2*({e}))'")
    print("With boundary conditions '({e} - Integer(1)).subs(x,1)'")

    print("Now you will be dropped to a IPython shell.")
    import IPython; IPython.embed()


# Variables that should be given as input:
#diffeq = "diff({e}, x) - (2*x-({e}))/x"
#bc = "({e}-Float(20.1)).subs(x,0.1)"
#interval = (0.1,1.0)

#entry_range=255
#chromo_length=20
#pop_size=1000
#m_chance=0.3
#rep_rate=.2
#pe_balance=100
